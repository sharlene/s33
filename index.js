console.log("Activity day 33");

/*
	API - https://jsonplaceholder.typicode.com/todos
	response.title / response.status

*/

//number 3
fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((data) => console.log(data))


//number 4
let dataArr = []
	fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((data) => {
		dataArr.push(data.map((objectElement) => {
			return objectElement.title
		}))
	})
	console.log(dataArr)


//number5
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => console.log(data))


//number6
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => {
		console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
	})


//number 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		body: "Created body of to do list item"
		userId: 1
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//number 8 - 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",	
		userId: 1
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//number 10 -11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "27/04/2023"
	})
})
	.then((response) => response.json())
	.then((data) => console.log(data))


//number 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'DELETE'
})